import React, { Component } from 'react';
class EditForm extends Component {
    constructor() {
        super();
        this.state = {
            id: 0,
            name: '',
            salary: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        let { id } = this.props.user;
        this.setState({
            id: id,
            name: "",
            salary: ""

        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.editar(this.state);
    }

    handleInputChange(e) {
        const { value, name } = e.target;
        console.log(value, name);
        this.setState({
            [name]: value
        });
    }


    render() {
        return (
            <div className="card">
                <form onSubmit={this.handleSubmit} className="card-body">
                    <div className="form-group">
                        <input
                            type="text"
                            name="name"
                            className="form-control"
                            value={this.state.name}
                            onChange={this.handleInputChange}
                            placeholder="Nombre"
                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="text"
                            name="salary"
                            className="form-control"
                            value={this.state.salary}
                            onChange={this.handleInputChange}
                            placeholder="Salario"
                        />
                    </div>

                    <button onClick={this.handleSubmit} className="btn btn-primary">
                        Editar
                    </button>
                </form>

            </div>
        )
    }

}

export default EditForm;
