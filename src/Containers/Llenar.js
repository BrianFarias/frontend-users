import React, { Component } from 'react';
import axios from 'axios';

class Llenar extends Component {
    constructor() {
        super();
        this.state = {
            users: []
        };
    }

    componentDidMount() {
        var _this = this;
        axios.get('http://localhost:9099/rest/users/index')
            .then(
                response => {
                    _this.setState(
                        {
                            users: response.data
                        }
                    );
                }
            )

            .catch(error => {
                console.log(error);
            }
            )
    }


    render() {

        const todos = this.state.users.map((user, i) => {
            return (
                <div className="col-md-4" key={user.id}>
                    <div className="card mt-4">
                        <div className="card-header"><h3>{user.name}</h3>
                            <span className="badge badge-pill badge-danger ml-2">{user.id}</span>
                        </div>
                        <div className="card-body">
                            <p>Salario: {user.salary}</p>
                        </div>
                    </div>
                </div>

            )
        });

        return (
            <div>
                <div className="Color container">
                    <div className="row mt-4">
                        {todos}
                    </div>
                </div>
            </div>

        )
    }
}

export default Llenar;