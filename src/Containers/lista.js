import React, { Component } from 'react';
import axios from 'axios';
import '../css/lista.css';
import UserForm from '../Componentes/Userform';
import EditForm from '../Componentes/Editform';
class Lista extends Component {
    constructor() {
        super();
        this.state = {
            users: []
        };
    }


    componentWillMount() {
        var _this = this;
        axios.get('http://localhost:9099/rest/users/index')
            .then(
                response => {
                    _this.setState(
                        {
                            users: response.data
                        }
                    );
                }
            )

            .catch(error => {
                console.log(error);
            }
            )
    }

    actualizar(user) {
        this.setState({
            users: [...this.state.users, user]
        })
    }

    guardaruser(user) {
        var _this = this;
        axios.post('http://localhost:9099/rest/users/index', user)
        .then(
            response => {
                _this.setState(
                    {
                        users: response.data
                    }
                );
            }
        )
            .catch(function (error) {
                
            });
    }

    deleteuser(user) {
        var _this = this;
        axios.delete('http://localhost:9099/rest/users/index/' + user.id, user.id)
            .then(response => {

                let usuarios = _this.state.users;

                usuarios = usuarios.filter((e, i) => {
                    return e.id !== user.id
                })
                _this.setState({ users: usuarios });
            });
    }

    editaruser(user) {
        var _this = this;
        axios.put('http://localhost:9099/rest/users/index/' + user.id, user)
            .then(
                response => {
                    let usuarios = _this.state.users;
                    usuarios = usuarios.filter((e, i) => {
                        if (e.id === user.id) {
                            e.name = user.name;
                            e.salary = user.salary;
                        }
                        return true;
                    })
                     _this.setState({ users: usuarios });
                }
            )
            .catch(function (error) {
                console.log(error);
            });
    }


    render() {

        const todos = this.state.users.map((user) => {
            return (
                <div className="col-md-4" key={user.id}>
                    <div className="card mt-4">
                        <div className="card-header"><h3>{user.name}</h3>
                        </div>
                        <div className="card-body">
                            <p>Salario:{user.salary}</p>
                            <p>ID:{user.id}</p>
                        </div>
                        <div className="container ">
                            <div className="row mt-4">
                                <button className="btn btn-info d-block mx-auto" >Editar</button>
                                <button className="btn btn-danger d-block mx-auto"
                                    onClick={this.deleteuser.bind(this, user)}
                                >Borrar</button>
                            </div>
                            <hr></hr>
                            <EditForm editar={this.editaruser.bind(this)} user={user}></EditForm>
                        </div>
                    </div>
                </div>

            )
        });

        return (
            <div>
                <div className="col-md-4 text-center">
                    <UserForm agregar={this.guardaruser.bind(this)}  ></UserForm>
                </div>
                <div className="Color container">
                    <div className="row mt-4">
                        {todos}
                    </div>
                </div>
            </div>

        )
    }

}
export default Lista;