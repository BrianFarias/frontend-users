import React, { Component } from 'react';
import logo from './logo.svg';
import './css/App.css';
import Lista from './Containers/lista';
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
           <Lista/>
           <hr></hr>
           <div></div>
        </header>
      </div>
    );
  }
}

export default App;
